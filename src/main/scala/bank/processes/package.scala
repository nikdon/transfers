package bank

package object processes {
  type Transfers[F[_]] = transfers.Processes[F]
  final val Transfers = transfers.Processes
}
