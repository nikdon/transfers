package bank.processes.transfers

import bank.domain.Timestamp
import bank.domain.transfers.{ AccountId, Event, Order, OrderId, State }
import bank.services.{ ReadTransfers, WriteTransfers }
import cats.Monad
import cats.implicits._

object Processes {
  def apply[F[_]]: Processes[F] =
    new Processes[F] {}
}

trait Processes[F[_]] {
  def state(
      accountId: AccountId
  )(
      implicit
      ReadTransfers: ReadTransfers[F]
  ): F[State.Account] =
    ReadTransfers.account(accountId)

  def logTransfersRegistered(
      accountId: AccountId
  )(
      implicit
      ReadTransfers: ReadTransfers[F]
  ): F[List[Event.TransferRegistered]] = ReadTransfers.logTransferRegistrations(accountId)

  def logTransferred(
      accountId: AccountId
  )(
      implicit
      ReadTransfers: ReadTransfers[F]
  ): F[List[Event.Transferred]] = ReadTransfers.logTransferred(accountId)

  def logDeposited(
      accountId: AccountId
  )(
      implicit
      ReadTransfers: ReadTransfers[F]
  ): F[List[Event.Deposited]] = ReadTransfers.logDeposited(accountId)

  def logWithdrawn(
      accountId: AccountId
  )(
      implicit
      ReadTransfers: ReadTransfers[F]
  ): F[List[Event.Withdrawn]] = ReadTransfers.logWithdrawn(accountId)

  def deposit(
      order: Order.Deposit
  )(
      implicit
      M: Monad[F],
      WriteTransfers: WriteTransfers[F],
      ReadTransfers: ReadTransfers[F]
  ) =
    for {
      e <- WriteTransfers.registerDeposit(order)
      deposited <- ReadTransfers.depositOrderState(order)
    } yield deposited

  def withdraw(
      order: Order.Withdraw
  )(
      implicit
      M: Monad[F],
      WriteTransfers: WriteTransfers[F],
      ReadTransfers: ReadTransfers[F]
  ) =
    for {
      _ <- WriteTransfers.registerWithdraw(order)
      withdrawn <- ReadTransfers.withdrawOrderState(order)
    } yield withdrawn

  def transfer(
      order: Order.Transfer
  )(
      implicit
      M: Monad[F],
      WriteTransfers: WriteTransfers[F],
      ReadTransfers: ReadTransfers[F]
  ): F[Event.Transferred] =
    for {
      // magic
      registered <- WriteTransfers.registerTransfer(order)
      withdrawn <- withdraw(Order.Withdraw(OrderId.random(), registered.order.from, registered.order.amount, Timestamp.now))
      deposited <- deposit(Order.Deposit(OrderId.random(), registered.order.to, registered.order.amount, Timestamp.now))
      transferred <- WriteTransfers.registerTransferred(order)
      confirmation <- ReadTransfers.transferOrderState(order)
    } yield confirmation
}
