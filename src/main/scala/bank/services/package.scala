package bank

import bank.services.transfers.write
import bank.services.transfers.read

package object services {
  type WriteTransfers[F[_]] = write.Algebra[F]
  final val WriteTransfers = write.Interpreters

  type ReadTransfers[F[_]] = read.Algebra[F]
  final val ReadTransfers = read.Interpreters
}
