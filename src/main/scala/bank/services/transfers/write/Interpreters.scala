package bank.services.transfers.write

import bank.main.{ ActorSystems, Executors }
import org.atnos.eff.Eff
import org.atnos.eff.future._

object Interpreters {
  def persistent[R: _future](systems: ActorSystems, executors: Executors): Algebra[Eff[R, ?]] =
    new Persistent[R](systems, executors)
}
