package bank.services.transfers.write

import akka.actor.{ Actor, ActorRef, ActorSystem, Props }
import akka.pattern.ask
import akka.persistence.PersistentActor
import akka.persistence.journal.{ Tagged, WriteEventAdapter }
import akka.util.Timeout
import bank.domain.transfers.Event.{ CommandFailure, Deposited, TransferRegistered, Transferred, Withdrawn }
import bank.domain.transfers._
import bank.domain.{ transfers, Timestamp }
import bank.main.{ ActorSystems, Executors }
import org.atnos.eff.Eff
import org.atnos.eff.future._

import scala.collection.concurrent.TrieMap
import scala.concurrent.duration._
import scala.concurrent.{ ExecutionContext, Future }

final class Persistent[R: _future](system: ActorSystems, executors: Executors) extends Algebra[Eff[R, ?]] {
  private implicit val _sys: ActorSystem = system.system
  private implicit val _ec: ExecutionContext = executors.default
  private implicit val timeout: Timeout = 2.seconds

  private val _accountActors = TrieMap.empty[AccountId, ActorRef]

  override def registerWithdraw(order: Order.Withdraw): Eff[R, Withdrawn] = {
    val ref = _accountActors.getOrElseUpdate(order.accountId, _sys.actorOf(AccountStateActor.props(order.accountId)))
    fromFuture(ask(ref, order).flatMap {
      case e: Event.Withdrawn => Future.successful(e)
      case t: transfers.Error => Future.failed(t)
    })
  }

  override def registerDeposit(order: Order.Deposit): Eff[R, Deposited] = {
    val ref = _accountActors.getOrElseUpdate(order.accountId, _sys.actorOf(AccountStateActor.props(order.accountId)))
    fromFuture(ask(ref, order).flatMap {
      case e: Event.Deposited => Future.successful(e)
      case t: transfers.Error => Future.failed(t)
    })
  }

  override def registerTransfer(order: Order.Transfer): Eff[R, TransferRegistered] = {
    val ref = _sys.actorOf(TransferOrderPersistentActor.props(order))
    fromFuture(ask(ref, TransferOrderPersistentActor.Register).flatMap {
      case e: Event.TransferRegistered => Future.successful(e)
      case t: transfers.Error          => Future.failed(t)
    })
  }

  override def registerTransferred(order: Order.Transfer): Eff[R, Event.Transferred] = {
    val ref = _sys.actorOf(TransferOrderPersistentActor.props(order))
    fromFuture(ask(ref, TransferOrderPersistentActor.Complete).flatMap {
      case e: Event.Transferred => Future.successful(e)
      case t: transfers.Error   => Future.failed(t)
    })
  }
}

class TransferTagger extends WriteEventAdapter {
  override def manifest(event: Any): String = ""
  override def toJournal(event: Any): Any = event match {
    case _: TransferRegistered =>
      Tagged(event, Set(TransferRegistered.getClass.getName))
    case _: Transferred        =>
      Tagged(event, Set(Transferred.getClass.getName))
    case _                     => event
  }
}

object TransferOrderPersistentActor {
  final case object Register
  final case object Complete

  def props(order: Order.Transfer): Props =
    Props(new TransferOrderPersistentActor(order))
}

class TransferOrderPersistentActor(order: Order.Transfer) extends PersistentActor {
  import TransferOrderPersistentActor._

  override def persistenceId: String = Order.Transfer.persistentId(order)

  override def receiveRecover: Receive = Actor.emptyBehavior

  override def receiveCommand: Receive = {
    case Register =>
      persist(Event.TransferRegistered(order, Timestamp.now)) { evt =>
        sender ! evt
        context.stop(self)
      }

    case Complete =>
      persist(Event.Transferred(order, Timestamp.now)) { evt =>
        sender ! evt
        context.stop(self)
      }
  }
}

private[write] object AccountStateActor {
  def props(id: AccountId): Props = Props(new AccountStateActor(id))
}

private[write] class AccountStateActor(id: AccountId) extends PersistentActor {

  override def persistenceId: String = id.value.toString

  var state = State.Account.init(id)

  def handle(order: Order): Either[transfers.Error, Event] =
    AccountState.handle(state, order).map {
      case (updated, event) =>
        state = updated
        event
    }

  def handle(event: Event): Unit = event match {
    case Deposited(order, _)      => state = State.Account(id, state.balance + order.amount.value)
    case Withdrawn(order, _)      => state = State.Account(id, state.balance - order.amount.value)
    case TransferRegistered(_, _) =>
    case Transferred(_, _)        =>
    case CommandFailure(_, _)     =>
  }

  override def receiveRecover: Receive = {
    case event: Event => handle(event)
    case _            => // skip
  }

  override def receiveCommand: Receive = {
    case order: Order =>
      val eventE = handle(order)
      eventE.fold(
        err => sender ! err,
        evt => persist(evt) { sender ! _ }
      )
  }
}

object AccountState {
  def handle(state: State.Account, order: Order): Either[transfers.Error, (State.Account, Event)] = order match {
    case o: Order.Deposit =>
      val updated = State.Account(state.id, state.balance + o.amount.value)
      Right(updated -> Deposited(o, Timestamp.now))

    case o: Order.Withdraw =>
      val opResult = state.balance - o.amount.value
      if (opResult >= 0) {
        val updated = State.Account(state.id, opResult)
        Right(updated -> Withdrawn(o, Timestamp.now))
      } else {
        Left(transfers.Error("Not enough funds", Some(CommandFailure(order, Timestamp.now))))
      }

    case other => Left(transfers.Error(s"Unexpected order: $other", Some(CommandFailure(other, Timestamp.now))))
  }
}
