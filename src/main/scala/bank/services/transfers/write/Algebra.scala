package bank.services.transfers.write

import bank.domain.transfers.{ Event, Order }

trait Algebra[F[_]] {
  def registerWithdraw(order: Order.Withdraw): F[Event.Withdrawn]
  def registerDeposit(order: Order.Deposit): F[Event.Deposited]
  def registerTransfer(order: Order.Transfer): F[Event.TransferRegistered]
  def registerTransferred(order: Order.Transfer): F[Event.Transferred]
}
