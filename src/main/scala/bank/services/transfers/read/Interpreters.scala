package bank.services.transfers.read

import bank.main.{ ActorSystems, Executors }
import org.atnos.eff.Eff
import org.atnos.eff.future._future

object Interpreters {
  def leveldbJournal[R: _future](systems: ActorSystems, executors: Executors): Algebra[Eff[R, ?]] =
    new LeveldbJournal[R](systems, executors)
}