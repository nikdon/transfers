package bank.services.transfers.read

import bank.domain.transfers.{ AccountId, Event, Order, State }

trait Algebra[F[_]] {
  def account(id: AccountId): F[State.Account]

  def logTransferRegistrations(accountId: AccountId): F[List[Event.TransferRegistered]]
  def logTransferred(accountId: AccountId): F[List[Event.Transferred]]
  def logDeposited(accountId: AccountId): F[List[Event.Deposited]]
  def logWithdrawn(accountId: AccountId): F[List[Event.Withdrawn]]

  def depositOrderState(order: Order.Deposit): F[Event.Deposited]
  def withdrawOrderState(order: Order.Withdraw): F[Event.Withdrawn]
  def transferOrderState(order: Order.Transfer): F[Event.Transferred]
}
