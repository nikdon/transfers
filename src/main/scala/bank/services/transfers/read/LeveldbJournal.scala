package bank.services.transfers.read

import akka.actor.ActorSystem
import akka.persistence.query.journal.leveldb.scaladsl.LeveldbReadJournal
import akka.persistence.query.{ EventEnvelope, PersistenceQuery }
import akka.stream.Materializer
import akka.stream.scaladsl.Sink
import bank.domain.transfers.{ AccountId, Event, Order, State }
import bank.main.{ ActorSystems, Executors }
import bank.services.transfers.write.AccountState
import org.atnos.eff.Eff
import org.atnos.eff.future.{ fromFuture, _ }

import scala.concurrent.duration._
import scala.concurrent.{ ExecutionContext, Future }

class LeveldbJournal[R: _future](system: ActorSystems, executors: Executors) extends Algebra[Eff[R, ?]] {
  private implicit val _sys: ActorSystem = system.system
  private implicit val _mat: Materializer = system.materializer
  private implicit val _ec: ExecutionContext = executors.default

  private val timeout = Some(10.seconds)

  val queries: LeveldbReadJournal =
    PersistenceQuery(_sys).readJournalFor[LeveldbReadJournal](LeveldbReadJournal.Identifier)

  override def logTransferRegistrations(accountId: AccountId): Eff[R, List[Event.TransferRegistered]] = {
    def query: Future[List[Event.TransferRegistered]] =
      queries
        .currentEventsByTag(Event.TransferRegistered.getClass.getName)
        .collect {
          case EventEnvelope(_, _, _, evt: Event.TransferRegistered)
              if evt.order.from == accountId || evt.order.to == accountId =>
            evt
        }
        .runWith(Sink.seq[Event.TransferRegistered])
        .map(_.toList)

    fromFuture(query, timeout)
  }

  override def logTransferred(accountId: AccountId): Eff[R, List[Event.Transferred]] = {
    def query: Future[List[Event.Transferred]] =
      queries
        .currentEventsByTag(Event.Transferred.getClass.getName)
        .collect {
          case EventEnvelope(_, _, _, evt: Event.Transferred)
              if evt.order.from == accountId || evt.order.to == accountId =>
            evt
        }
        .runWith(Sink.seq[Event.Transferred])
        .map(_.toList)

    fromFuture(query, timeout)
  }

  override def logDeposited(accountId: AccountId): Eff[R, List[Event.Deposited]] = {
    def query: Future[List[Event.Deposited]] =
      queries
        .currentEventsByPersistenceId(accountId.value.toString)
        .collect {
          case EventEnvelope(_, _, _, evt: Event.Deposited) => evt
        }
        .runWith(Sink.seq[Event.Deposited])
        .map(_.toList)

    fromFuture(query, timeout)
  }

  override def logWithdrawn(accountId: AccountId): Eff[R, List[Event.Withdrawn]] = {
    def query: Future[List[Event.Withdrawn]] =
      queries
        .currentEventsByPersistenceId(accountId.value.toString)
        .collect {
          case EventEnvelope(_, _, _, evt: Event.Withdrawn) => evt
        }
        .runWith(Sink.seq[Event.Withdrawn])
        .map(_.toList)

    fromFuture(query, timeout)
  }

  override def account(accountId: AccountId): Eff[R, State.Account] = {
    def query: Future[State.Account] =
      queries
        .currentEventsByPersistenceId(accountId.value.toString)
        .fold(State.Account.init(accountId)) {
          case (acc, EventEnvelope(_, _, _, evt: Event)) =>
            AccountState.handle(acc, evt.order).fold(_ => acc, _._1)
        }
        .runWith(Sink.head)

    fromFuture(query, timeout)
  }

  override def depositOrderState(order: Order.Deposit): Eff[R, Event.Deposited] = {
    fromFuture(
      queries
        .eventsByPersistenceId(order.accountId.value.toString)
        .collect {
          case EventEnvelope(_, _, _, evt @ Event.Deposited(`order`, _)) => evt
        }
        .runWith(Sink.head),
      timeout
    )
  }

  override def withdrawOrderState(order: Order.Withdraw): Eff[R, Event.Withdrawn] =
    fromFuture(
      queries
        .eventsByPersistenceId(order.accountId.value.toString)
        .collect {
          case EventEnvelope(_, _, _, evt @ Event.Withdrawn(`order`, _)) => evt
        }
        .runWith(Sink.head),
      timeout
    )

  override def transferOrderState(order: Order.Transfer): Eff[R, Event.Transferred] =
    fromFuture(
      queries
        .eventsByPersistenceId(Order.Transfer.persistentId(order))
        .collect {
          case EventEnvelope(_, _, _, evt @ Event.Transferred(`order`, _)) => evt
        }
        .runWith(Sink.head),
      timeout
    )
}
