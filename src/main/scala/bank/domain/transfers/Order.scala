package bank.domain.transfers

import java.util.UUID

import bank.domain.Timestamp
import eu.timepit.refined.api.Refined
import eu.timepit.refined.numeric.NonNegative
import io.circe.generic.JsonCodec
import io.circe.generic.extras.wrapped.{ deriveUnwrappedDecoder, deriveUnwrappedEncoder }
import io.circe.refined._
import io.circe.{ Decoder, Encoder }

@JsonCodec sealed trait Order extends Product with Serializable {
  def id: OrderId
}
object Order {
  @JsonCodec final case class Transfer(
      id: OrderId,
      from: AccountId,
      to: AccountId,
      amount: BigDecimal Refined NonNegative,
      timestamp: Timestamp
  ) extends Order

  object Transfer {
    def persistentId(order: Order.Transfer): String = {
      s"${order.from}:${order.to}"
    }
  }

  @JsonCodec final case class Deposit(
      id: OrderId,
      accountId: AccountId,
      amount: BigDecimal Refined NonNegative,
      timestamp: Timestamp
  ) extends Order

  @JsonCodec final case class Withdraw(
      id: OrderId,
      accountId: AccountId,
      amount: BigDecimal Refined NonNegative,
      timestamp: Timestamp
  ) extends Order
}

final case class OrderId(value: UUID) extends AnyVal
object OrderId {
  def random() = OrderId(UUID.randomUUID())

  implicit val encoder: Encoder[OrderId] = deriveUnwrappedEncoder[OrderId]
  implicit val decoder: Decoder[OrderId] = deriveUnwrappedDecoder[OrderId]
}
