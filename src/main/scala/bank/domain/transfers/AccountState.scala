package bank.domain.transfers

import java.util.UUID

import io.circe.generic.JsonCodec
import io.circe.generic.extras.wrapped.{ deriveUnwrappedDecoder, deriveUnwrappedEncoder }
import io.circe.{ Decoder, Encoder }

sealed trait State extends Product with Serializable
object State {
  @JsonCodec final case class Account(id: AccountId, balance: BigDecimal) extends State
  object Account {
    def init(id: AccountId) = Account(id, 0)
  }
}

final case class AccountId(value: UUID) extends AnyVal
object AccountId {
  def random(): AccountId = AccountId.fromString(UUID.randomUUID().toString)
  def fromString(name: String): AccountId = AccountId(UUID.fromString(name))

  implicit val encoder: Encoder[AccountId] = deriveUnwrappedEncoder[AccountId]
  implicit val decoder: Decoder[AccountId] = deriveUnwrappedDecoder[AccountId]
}
