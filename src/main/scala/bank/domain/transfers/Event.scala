package bank.domain.transfers

import bank.domain.Timestamp
import io.circe.generic.JsonCodec
import io.circe.refined._

@JsonCodec sealed trait Event extends Product with Serializable {
  def order: Order
  def timestamp: Timestamp
}
object Event {
  @JsonCodec final case class Deposited(order: Order.Deposit, timestamp: Timestamp) extends Event
  @JsonCodec final case class Withdrawn(order: Order.Withdraw, timestamp: Timestamp) extends Event
  @JsonCodec final case class TransferRegistered(order: Order.Transfer, timestamp: Timestamp) extends Event
  @JsonCodec final case class Transferred(order: Order.Transfer, timestamp: Timestamp) extends Event
  @JsonCodec final case class CommandFailure(order: Order, timestamp: Timestamp) extends Event
}
