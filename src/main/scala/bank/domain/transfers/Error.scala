package bank.domain.transfers

import scala.util.control.NoStackTrace

final case class Error(message: String, event: Option[Event]) extends Throwable with NoStackTrace {
  override def getMessage: String = message
}
