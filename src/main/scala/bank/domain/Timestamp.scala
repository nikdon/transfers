package bank.domain

import io.circe._
import io.circe.generic.extras.wrapped._
import io.circe.java8.time._
import java.time.{ Instant, OffsetDateTime, ZoneId }

case class Timestamp(value: OffsetDateTime) extends AnyVal

object Timestamp {
  def now: Timestamp =
    Timestamp(OffsetDateTime.now)

  def ofEpochSecond(epochSecond: Long, zoneId: ZoneId) =
    Timestamp(OffsetDateTime.ofInstant(Instant.ofEpochSecond(epochSecond), zoneId))

  implicit val encoder: Encoder[Timestamp] =
    deriveUnwrappedEncoder[Timestamp]

  implicit val decoder: Decoder[Timestamp] =
    Decoder[Long].map(ofEpochSecond(_, ZoneId.of("UTC"))) or
      io.circe.java8.time.decodeOffsetDateTimeDefault.map(Timestamp(_)) // for integration tests
}
