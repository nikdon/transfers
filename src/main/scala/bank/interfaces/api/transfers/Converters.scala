package bank.interfaces.api.transfers

import bank.domain.Timestamp
import bank.domain.transfers.{ Event, Order, OrderId, State }
import bank.interfaces.api.transfers.Protocol.{
  DepositApiRequest,
  DepositApiResponse,
  TransferApiRequest,
  TransferApiResponse,
  WithdrawApiRequest,
  WithdrawApiResponse
}

object Converters {
  def fromTransferApiRequest(
      request: TransferApiRequest
  ): Order.Transfer =
    Order.Transfer(OrderId.random(), request.from, request.to, request.amount, Timestamp.now)

  def toTransferApiResponse(
      event: Event.Transferred
  ): TransferApiResponse = Protocol.TransferApiResponse(event)

  def toTransferHistoryApiResponse(
      state: State.Account,
      intents: List[Event.TransferRegistered],
      completed: List[Event.Transferred]
  ): Protocol.TransfersHistoryApiResponse =
    Protocol.TransfersHistoryApiResponse(state, intents, completed)

  def fromDepositApiRequest(
      request: DepositApiRequest
  ): Order.Deposit =
    Order.Deposit(OrderId.random(), request.accountId, request.amount, Timestamp.now)

  def toDepositApiResponse(
      event: Event.Deposited
  ): DepositApiResponse = Protocol.DepositApiResponse(event)

  def toDepositHistoryApiResponse(
      state: State.Account,
      completed: List[Event.Deposited]
  ): Protocol.DepositHistoryApiResponse =
    Protocol.DepositHistoryApiResponse(state, completed)

  def fromWithdrawApiRequest(
      request: WithdrawApiRequest
  ): Order.Withdraw =
    Order.Withdraw(OrderId.random(), request.accountId, request.amount, Timestamp.now)

  def toWithdrawApiResponse(
      event: Event.Withdrawn
  ): WithdrawApiResponse = Protocol.WithdrawApiResponse(event)

  def toWithdrawHistoryApiResponse(
      state: State.Account,
      completed: List[Event.Withdrawn]
  ): Protocol.WithdrawHistoryApiResponse =
    Protocol.WithdrawHistoryApiResponse(state, completed)
}
