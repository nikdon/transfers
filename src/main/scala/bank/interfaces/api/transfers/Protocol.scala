package bank.interfaces.api.transfers

import bank.domain.transfers.{ AccountId, Event, State }
import eu.timepit.refined.api.Refined
import eu.timepit.refined.numeric.NonNegative
import io.circe.generic.JsonCodec
import io.circe.refined._

object Protocol {
  @JsonCodec final case class TransferApiRequest(
      from: AccountId,
      to: AccountId,
      amount: BigDecimal Refined NonNegative
  )

  @JsonCodec final case class TransferApiResponse(
      result: Event.Transferred
  )

  @JsonCodec final case class TransfersHistoryApiResponse(
      account: State.Account,
      transferIntents: List[Event.TransferRegistered],
      completedTransfers: List[Event.Transferred]
  )

  @JsonCodec final case class DepositApiRequest(
    accountId: AccountId,
    amount: BigDecimal Refined NonNegative
  )

  @JsonCodec final case class DepositApiResponse(
    result: Event.Deposited
  )

  @JsonCodec final case class DepositHistoryApiResponse(
    account: State.Account,
    completed: List[Event.Deposited]
  )

  @JsonCodec final case class WithdrawApiRequest(
    accountId: AccountId,
    amount: BigDecimal Refined NonNegative
  )

  @JsonCodec final case class WithdrawApiResponse(
    result: Event.Withdrawn
  )

  @JsonCodec final case class WithdrawHistoryApiResponse(
    account: State.Account,
    completed: List[Event.Withdrawn]
  )
}
