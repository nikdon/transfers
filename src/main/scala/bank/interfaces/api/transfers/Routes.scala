package bank.interfaces.api.transfers

import java.util.UUID

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server
import bank.config.ApplicationConfig
import bank.domain.transfers.AccountId
import bank.interfaces.api.transfers.Protocol.{ DepositApiRequest, TransferApiRequest, WithdrawApiRequest }
import bank.main.{ Processes, Runners }
import cats.implicits._
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.ErrorAccumulatingCirceSupport
import org.zalando.grafter.macros.readerOf

@readerOf[ApplicationConfig]
case class Routes(
    processes: Processes,
    runners: Runners
) extends LazyLogging
    with ErrorAccumulatingCirceSupport {
  import Converters._
  import processes._
  import server.Directives._

  val transfer: server.Route = redirectToNoTrailingSlashIfPresent(StatusCodes.Found) {
    concat(
      pathEnd {
        post {
          entity(as[TransferApiRequest]) { req =>
            complete {
              runners.runApp(Transfers.transfer(fromTransferApiRequest(req)).map(toTransferApiResponse))
            }
          }
        }
      },
      pathEnd {
        get {
          parameter('id) { id =>
            complete {
              val accountId = AccountId.fromString(id)
              runners.runApp {
                (
                  Transfers.state(accountId),
                  Transfers.logTransfersRegistered(accountId),
                  Transfers.logTransferred(accountId)
                ).mapN {
                  case (state, transferIntents, completed) =>
                    toTransferHistoryApiResponse(state, transferIntents, completed)
                }
              }
            }
          }
        }
      }
    )
  }

  val deposit: server.Route = redirectToNoTrailingSlashIfPresent(StatusCodes.Found) {
    concat(
      pathEnd {
        post {
          entity(as[DepositApiRequest]) { req =>
            complete {
              runners.runApp(Transfers.deposit(fromDepositApiRequest(req)).map(toDepositApiResponse))
            }
          }
        }
      },
      pathEnd {
        get {
          parameter('id) { id =>
            complete {
              val accountId = AccountId.fromString(id)
              runners.runApp {
                (
                  Transfers.state(accountId),
                  Transfers.logDeposited(accountId)
                ).mapN {
                  case (state, completed) => toDepositHistoryApiResponse(state, completed)
                }
              }
            }
          }
        }
      }
    )
  }

  val withdrawal: server.Route = redirectToNoTrailingSlashIfPresent(StatusCodes.Found) {
    concat(
      pathEnd {
        post {
          entity(as[WithdrawApiRequest]) { req =>
            complete {
              runners.runApp(Transfers.withdraw(fromWithdrawApiRequest(req)).map(toWithdrawApiResponse))
            }
          }
        }
      },
      pathEnd {
        get {
          parameter('id) { id =>
            complete {
              val accountId = AccountId.fromString(id)
              runners.runApp {
                (
                  Transfers.state(accountId),
                  Transfers.logWithdrawn(accountId)
                ).mapN {
                  case (state, completed) => toWithdrawHistoryApiResponse(state, completed)
                }
              }
            }
          }
        }
      }
    )
  }

}
