package bank.interfaces.api.utils

import akka.http.scaladsl._
import akka.http.scaladsl.model.StatusCodes
import bank.domain.transfers
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.JsonCodec
import io.circe.generic.extras.auto._

object ApiExceptionHandler extends FailFastCirceSupport {

  @JsonCodec final case class RejectionResponse(error: Boolean, message: String, service: String)

  def apply(): server.ExceptionHandler =
    server.ExceptionHandler {
      case err: transfers.Error ⇒
        ctx ⇒
          ctx.complete((StatusCodes.InternalServerError, RejectionResponse(error = true, err.getMessage, "transfers")))

      case err: Throwable ⇒
        ctx ⇒
          ctx.complete((StatusCodes.InternalServerError, RejectionResponse(error = true, err.getMessage, "system")))
    }

}
