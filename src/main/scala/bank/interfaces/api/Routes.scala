package bank.interfaces.api

import akka.http.scaladsl._
import bank.config._
import org.zalando.grafter.macros._
import utils._

@readerOf[ApplicationConfig]
case class Routes(
    transfersRoutes: transfers.Routes
) {
  import server.Directives._

  lazy val route: server.Route =
    handleExceptions(ApiExceptionHandler()) {
      handleRejections(ApiRejectionHandler()) {
        pathPrefix("api" / "v1") {
          concat(
            pathPrefix("deposit") { transfersRoutes.deposit },
            pathPrefix("transfer") { transfersRoutes.transfer },
            pathPrefix("withdrawal") { transfersRoutes.withdrawal }
          )
        }
      }
    }
}
