package bank.main

import bank.config._
import org.atnos.eff.syntax.future._
import org.atnos.eff.{ Eff, ExecutorServices }
import org.zalando.grafter.macros._

import scala.concurrent.Future

@readerOf[ApplicationConfig]
case class Runners(executors: Executors) {

  private implicit val ec = executors.runner
  private implicit val sec = ExecutorServices.fromExecutionContext(ec).scheduledExecutorService
  private implicit val scheduler = ExecutorServices.schedulerFromScheduledExecutorService(sec)

  def runApp[R](
      app: Eff[AppStack, R]
  ): Future[R] =
    app.runAsync
}
