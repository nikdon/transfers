package bank.main

import bank.config._
import bank.{ processes ⇒ p, services ⇒ s }
import org.zalando.grafter.macros._

@readerOf[ApplicationConfig]
case class Processes(actorSystems: ActorSystems, executors: Executors) {
  implicit val _writeTransfers: s.WriteTransfers[AppEffect] =
    s.WriteTransfers.persistent[AppStack](actorSystems, executors)

  implicit val _readTransfers: s.ReadTransfers[AppEffect] =
    s.ReadTransfers.leveldbJournal[AppStack](actorSystems, executors)

  final val Transfers = p.Transfers[AppEffect]
}
