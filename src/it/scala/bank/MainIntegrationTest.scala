package bank

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import bank.domain.transfers.AccountId
import bank.interfaces.api.transfers.Protocol._
import de.heikoseeberger.akkahttpcirce.ErrorAccumulatingCirceSupport
import eu.timepit.refined.api.Refined
import eu.timepit.refined.numeric._
import org.scalacheck.Gen
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{ BeforeAndAfterAll, FlatSpec, Matchers }

class MainIntegrationTest
    extends FlatSpec
    with Matchers
    with ScalatestRouteTest
    with BeforeAndAfterAll
    with ErrorAccumulatingCirceSupport
    with GeneratorDrivenPropertyChecks {

  val main: Main.type = Main

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    main.main(Array.empty)
  }

  behavior of "App"

  it should "keep account balance eq 0.0 after deposit and withdrawal of the same amount" in forAll(Gen.posNum[Double]) {
    amount =>
      val app = main.app.fold(fail("App does not exist"))(identity)

      val accId = AccountId.random()
      val deposit: BigDecimal Refined NonNegative = Refined.unsafeApply(BigDecimal(amount))
      val depositRequest = DepositApiRequest(accId, deposit)
      val withdrawRequest = WithdrawApiRequest(accId, deposit)

      Post("/api/v1/deposit", depositRequest) ~> app.api.routes.route ~> check {
        response.status shouldBe StatusCodes.OK
        val res = responseAs[DepositApiResponse]
        res shouldBe a[DepositApiResponse]

        Get(s"/api/v1/deposit?id=${accId.value.toString}") ~> app.api.routes.route ~> check {
          response.status shouldBe StatusCodes.OK
          val res = responseAs[DepositHistoryApiResponse]
          res.account.id shouldBe accId
          res.account.balance.compareTo(deposit.value) shouldBe 0
          res.completed.lengthCompare(1) shouldBe 0
        }

        Post("/api/v1/withdrawal", withdrawRequest) ~> app.api.routes.route ~> check {
          response.status shouldBe StatusCodes.OK
          val res = responseAs[WithdrawApiResponse]
          res shouldBe a[WithdrawApiResponse]
        }

        Get(s"/api/v1/withdrawal?id=${accId.value.toString}") ~> app.api.routes.route ~> check {
          response.status shouldBe StatusCodes.OK
          val res = responseAs[DepositHistoryApiResponse]
          res.account.id shouldBe accId
          res.account.balance.compareTo(BigDecimal(0)) shouldBe 0
          res.completed.lengthCompare(1) shouldBe 0
        }
      }
  }

  it should "withdraw `from` account and deposit `to` account by transfer" in forAll(Gen.posNum[Double]) {
    amount =>
      val app = main.app.fold(fail("App does not exist"))(identity)

      val from = AccountId.random()
      val to = AccountId.random()

      val transferAmount: BigDecimal Refined NonNegative = Refined.unsafeApply(BigDecimal(amount))

      val depositRequest = DepositApiRequest(from, transferAmount)
      val transferApiRequest = TransferApiRequest(from, to, transferAmount)

      Post("/api/v1/deposit", depositRequest) ~> app.api.routes.route ~> check {
        response.status shouldBe StatusCodes.OK
        val res = responseAs[DepositApiResponse]
        res shouldBe a[DepositApiResponse]

        Post("/api/v1/transfer", transferApiRequest) ~> app.api.routes.route ~> check {
          response.status shouldBe StatusCodes.OK
          val res = responseAs[TransferApiResponse]
          res shouldBe a[TransferApiResponse]

          Get(s"/api/v1/deposit?id=${to.value.toString}") ~> app.api.routes.route ~> check {
            response.status shouldBe StatusCodes.OK
            val res = responseAs[DepositHistoryApiResponse]
            res.account.id shouldBe to
            res.account.balance.compareTo(transferAmount.value) shouldBe 0
            res.completed.lengthCompare(1) shouldBe 0
          }

          Get(s"/api/v1/withdrawal?id=${from.value.toString}") ~> app.api.routes.route ~> check {
            response.status shouldBe StatusCodes.OK
            val res = responseAs[DepositHistoryApiResponse]
            res.account.id shouldBe from
            res.account.balance.compareTo(BigDecimal(0)) shouldBe 0
            res.completed.lengthCompare(1) shouldBe 0
          }
        }

        Get(s"/api/v1/transfer?id=${from.value.toString}") ~> app.api.routes.route ~> check {
          response.status shouldBe StatusCodes.OK
          val res = responseAs[TransfersHistoryApiResponse]
          res.account.id shouldBe from
          res.account.balance.compareTo(BigDecimal(0)) shouldBe 0
          res.transferIntents.lengthCompare(1) shouldBe 0
          res.completedTransfers.lengthCompare(1) shouldBe 0
        }
      }
  }
}
