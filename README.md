# transfers

Money transfers between accounts.

## Quickstart

Build a docker image and start service:

```shell
$ sbt docker:publishLocal
$ docker run -d -p 8080:8080 transfers:0.1.0
$ curl -X GET "http://$(docker-machine ip):8080/api/v1/withdrawal?id=<UUID>"
```

## API

All endpoints start with `api/v<int>` where <int> is version of the api. Current version is `api/v1`

### API Errors

For all endpoints API can return errors in the format in case of any errors:

```json
{
    "error": true,
    "message": "Not enough funds",
    "service": "transfers"
}
```

### Deposit `/api/v1/deposit`

| Endpoint      | Method | Description                                                 | 
|---------------|--------|-------------------------------------------------------------| 
| /             | POST   | Sends command to deposits funds to the given account        |
| /?id=`<UUID>` | GET    | Returns current account state and history of deposits       |

#### **POST** `/api/v1/deposit`

Sends command to deposits funds to the given account:

```bash
curl -X POST \
   http://$(docker-machine ip):8080/api/v1/deposit \
   -H 'Content-Type: application/json' \
   -d '{
        "accountId": "64a79dd6-86cc-4377-8443-8e7762fc6771",
        "amount": 16.0
   }' | jq .
   
{   
    "result": {
        "order": {
          "id": "e21de087-2391-46fe-8c3e-c0cd1d2e50c6",
          "accountId": "64a79dd6-86cc-4377-8443-8e7762fc6771",
          "amount": 20,
          "timestamp": "2018-07-13T08:43:33.1+03:00"
        },
        "timestamp": "2018-07-13T08:43:33.143+03:00"
    }
}
```

#### **GET** `/api/v1/deposit?id=<uuid>`

Returns for the given account id current state and history of deposits:

```bash
curl -X GET "http://$(docker-machine ip):8080/api/v1/deposit?id=64a79dd6-86cc-4377-8443-8e7762fc6771" | jq .

{
    "account": {
        "id": "64a79dd6-86cc-4377-8443-8e7762fc6771",
        "balance": 20
    },
    "completed": [
        {
            "order": {
                "id": "4f8c25bc-1fd6-4cd6-9139-7d5ed9188a9b",
                "accountId": "64a79dd6-86cc-4377-8443-8e7762fc6771",
                "amount": 20,
                "timestamp": "2018-07-12T15:52:45.812+03:00"
            },
            "timestamp": "2018-07-12T15:52:46.081+03:00"
        }
    ]
}
```

### Withdrawal `/api/v1/withdrawal`

| Endpoint      | Method | Description                                                 | 
|---------------|--------|-------------------------------------------------------------| 
| /             | POST   | Sends command to withdraw funds from the account            |
| /?id=`<UUID>` | GET    | Returns current account state and history of withdrawals    |

#### **POST** `/api/v1/withdrawal`

Sends command to withdraw funds from the account:

```bash
curl -X POST \
   http://$(docker-machine ip):8080/api/v1/withdrawal \
   -H 'Content-Type: application/json' \
   -d '{
        "accountId": "64a79dd6-86cc-4377-8443-8e7762fc6771",
        "amount": 4.0
   }' | jq .
   
{
    "result": {
        "order": {
            "id": "9c4ef240-2d02-4db9-898c-0e2a4adaa236",
            "accountId": "64a79dd6-86cc-4377-8443-8e7762fc6771",
            "amount": 4,
            "timestamp": "2018-07-13T08:50:39.681+03:00"
        },
        "timestamp": "2018-07-13T08:50:39.684+03:00"
    }
}
```

#### **GET** `/api/v1/withdrawal?id=<uuid>`

Returns current account state and history of withdrawals:

```bash
curl -X GET "http://$(docker-machine ip):8080/api/v1/withdrawal?id=64a79dd6-86cc-4377-8443-8e7762fc6771" | jq .

{
    "account": {
        "id": "64a79dd6-86cc-4377-8443-8e7762fc6771",
        "balance": 16
    },
    "completed": [
        {
            "order": {
                "id": "a2ca26f2-ad79-45a6-aa14-6a124c3cd8a9",
                "accountId": "64a79dd6-86cc-4377-8443-8e7762fc6771",
                "amount": 4,
                "timestamp": "2018-07-12T15:52:53.472+03:00"
            },
            "timestamp": "2018-07-12T15:52:53.475+03:00"
        }
    ]
}
```

### Transfer `/api/v1/transfer`

| Endpoint      | Method | Description                                                 | 
|---------------|--------|-------------------------------------------------------------| 
| /             | POST   | Sends command to transfer funds from one account to another |
| /?id=`<UUID>` | GET    | Returns current account state and history of transactions   |

#### **POST** `/api/v1/transfer`

Sends command to transfer funds from one account to another:

```bash
curl -X POST \
  http://$(docker-machine ip):8080/api/v1/transfer \
  -H 'Content-Type: application/json' \
  -d '{
	"from": "64a79dd6-86cc-4377-8443-8e7762fc6771",
	"to": "23e1cf16-19cf-490d-abcf-0ad17b939887",
	"amount": 2.0
}' | jq .

{
    "result": {
        "order": {
            "id": "14562f31-85c7-47d6-a71b-4b0dc4ad8b7b",
            "from": "64a79dd6-86cc-4377-8443-8e7762fc6771",
            "to": "23e1cf16-19cf-490d-abcf-0ad17b939887",
            "amount": 2,
            "timestamp": "2018-07-13T08:54:06.353+03:00"
        },
        "timestamp": "2018-07-13T08:54:06.413+03:00"
    }
}
```

#### **GET** `/api/v1/transfer?id=<uuid>`

Returns current account state and history of transactions:

```bash
curl -X GET "http://$(docker-machine ip):8080/api/v1/transfer?id=64a79dd6-86cc-4377-8443-8e7762fc6771" | jq .

{
    "account": {
        "id": "64a79dd6-86cc-4377-8443-8e7762fc6771",
        "balance": 14
    },
    "transferIntents": [
        {
            "order": {
                "id": "14562f31-85c7-47d6-a71b-4b0dc4ad8b7b",
                "from": "64a79dd6-86cc-4377-8443-8e7762fc6771",
                "to": "23e1cf16-19cf-490d-abcf-0ad17b939887",
                "amount": 2,
                "timestamp": "2018-07-13T08:54:06.353+03:00"
            },
            "timestamp": "2018-07-13T08:54:06.379+03:00"
        }
    ],
    "completedTransfers": [
        {
            "order": {
                "id": "14562f31-85c7-47d6-a71b-4b0dc4ad8b7b",
                "from": "64a79dd6-86cc-4377-8443-8e7762fc6771",
                "to": "23e1cf16-19cf-490d-abcf-0ad17b939887",
                "amount": 2,
                "timestamp": "2018-07-13T08:54:06.353+03:00"
            },
            "timestamp": "2018-07-13T08:54:06.413+03:00"
        }
    ]
}
```

## Local run and tests

To run a service with sbt:

```shell
$ sbt run
```

To run integration tests:

```shell
$ sbt it:test
```

## About

### General notes

1. The service returns `json` entities.

2. The service does not include security and CORS headers.

3. The service works with local java implementation of the LevelDB.
